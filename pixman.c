#include <pixman.h>
#include <stdio.h>

// struct pixman_region32_data {
//     long		size;
//     long		numRects;
// /*  pixman_box32_t	rects[size];   in memory but not explicitly declared */
// };

void print(pixman_region32_t *region) {
  printf("(%3d,%3d) (%3d,%3d)\n", region->extents.x1, region->extents.y1,
         region->extents.x2, region->extents.y2);
}

int main() {
  pixman_region32_t region1;
  pixman_region32_init_rect(&region1, 10, 0, 20, 10);
  print(&region1);

  pixman_region32_t region2;
  pixman_region32_init_rect(&region2, 30, 40, 20, 10);
  print(&region2);

  pixman_region32_t region3;
  pixman_region32_init(&region3);
  pixman_region32_union_rect(&region3, &region1, 30, 40, 20, 10);
  print(&region3);

  pixman_region32_t region4;
  pixman_region32_init(&region4);
  pixman_region32_subtract(&region4, &region3, &region2);
  print(&region4);

  pixman_region32_fini(&region1);
  pixman_region32_fini(&region2);
  pixman_region32_fini(&region3);
  pixman_region32_fini(&region4);
  
  return 0;
}
