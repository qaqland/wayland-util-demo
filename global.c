#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <wayland-server-core.h>
#include <wayland-server-protocol.h>

const struct wl_compositor_interface if_impl = {0};

// wl_compositor 是单例对象，所以即使 client 断开也不会执行这里
void resource_destroy(struct wl_resource *resource) {
  int *count = wl_resource_get_user_data(resource);
  printf("[compositor resource destroy] get data: %d\n", *count);
  exit(EXIT_FAILURE);
}

void global_bind(struct wl_client *client, void *data, uint32_t version,
                 uint32_t id) {
  int *count = data;
  printf("[compositor global bind] get data: %d\n", *count);

  struct wl_resource *resource =
      wl_resource_create(client, &wl_compositor_interface, version, id);
  assert(resource);
  wl_resource_set_implementation(resource, &if_impl, count, resource_destroy);
  exit(EXIT_FAILURE);
};

int main() {
  struct wl_display *display = wl_display_create();
  const char *socket = wl_display_add_socket_auto(display);
  assert(socket);

  // state or data
  int count = 1234;

  // globals
  printf("[compositor global create] wl_compositor\n");
  wl_global_create(display, &wl_compositor_interface, 4, &count, global_bind);

  wl_display_run(display);
  return 0;
}

// run client like weston-simple-egl to bind global
