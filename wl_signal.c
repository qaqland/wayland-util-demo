#include <stdio.h>
#include <wayland-server-core.h>

struct wl_signal signal;

struct part {
  int node;
  struct wl_listener my_listener;
};

void handle_signal(struct wl_listener *listener, void *data) {
  struct part *this_part;
  this_part = wl_container_of(listener, this_part, my_listener);
  printf("part %d get → %s\n", this_part->node, (char *)data);
}

int main(void) {

  wl_signal_init(&signal);

  // add part 1
  struct part part1 = {.node = 1};
  part1.my_listener.notify = handle_signal;
  wl_signal_add(&signal, &part1.my_listener);

  // signal 1
  printf(">>> send signal 1\n");
  wl_signal_emit(&signal, (void *)"hello (1)");

  // add part 2
  struct part part2 = {.node = 2};
  part2.my_listener.notify = handle_signal;
  wl_signal_add(&signal, &part2.my_listener);

  // signal 2
  printf(">>> send signal 2\n");
  wl_signal_emit(&signal, (void *)"hello (2)");

  // del part 1
  wl_list_remove(&part1.my_listener.link);

  // signal 3
  printf(">>> send signal 3\n");
  wl_signal_emit(&signal, (void *)"hello (3)");

  return 0;
}
